import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'turkish_restaurant',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const MyHomePage(title: 'TURKISH RESTAURANTS'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Widget> restaurant = [];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            widget.title,
            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w900),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              createGridCategory(),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
                child: Transform(
                  transform: Matrix4.translationValues(-50, 0, 0),
                  child: const Text(
                    'MOST POPULAR RESTAURANTS',
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 250,
                child: createRestaurantLisViewHorizontal(),
              ),
              makeGridRestaurant(),
            ],
          ),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }

  Widget createGridCategory() {
    return GridView(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        childAspectRatio: 2,
        crossAxisSpacing: 1,
        mainAxisSpacing: 1,
      ),
      children: [
        category(Icons.temple_buddhist_outlined, 'TURKISH'),
        category(Icons.kitchen_outlined, 'ARABIC'),
        category(Icons.dining_outlined, 'AMERICAN'),
        category(Icons.rice_bowl_outlined, 'ASIAN'),
        category(Icons.cake_outlined, 'BURGER'),
        category(Icons.breakfast_dining_outlined, 'BREAKFAST'),
        category(Icons.coffee_outlined, 'COFFEE'),
        category(Icons.lunch_dining_outlined, 'DESSERTS'),
        category(Icons.local_pizza_outlined, 'PIZZA'),
        category(Icons.icecream_outlined, 'SEA FOOD'),
      ],
    );
  }

  Widget category(var icon, var text) {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
      decoration: BoxDecoration(
          border: Border.all(
        color: Colors.grey.shade300,
      )),
      child: Column(
        children: [
          Icon(
            icon,
            size: 20,
          ),
          Text(
            text,
            style: const TextStyle(fontSize: 10),
          ),
        ],
      ),
    );
  }

  Widget generatingStar() {
    List<Widget> star = [];
    for (var i = 0; i < 4; i++) {
      star.add(
        const Icon(
          Icons.star,
          size: 15,
          color: Colors.orangeAccent,
        ),
      );
    }
    star.add(
      const Icon(
        Icons.star_half,
        size: 15,
        color: Colors.orangeAccent,
      ),
    );
    return Row(children: star);
  }

  List<Widget> makeRestaurantList() {
    if (restaurant.length < 10) {
      for (int i = 0; i < 4; i++) {
        restaurant.add(makeRestaurantsInfo(
            'assets/images/restaurant1.jpg',
            'JWMariot',
            'That way you do not have to enable This method will '
                'come in handy if you have objects'));
        restaurant.add(makeRestaurantsInfo(
            'assets/images/restaurant_2.jpg',
            'HanoiRiot',
            'That way you do not have to enable This method will '
                'come in handy if you have objects'));
        restaurant.add(makeRestaurantsInfo(
            'assets/images/restaurant_3.jpg',
            'Michellin',
            'That way you do not have to enable This method will '
                'come in handy if you have objects'));
      }
    }
    return restaurant;
  }

  Widget createRestaurantLisViewHorizontal() {
    return ListView(
      scrollDirection: Axis.horizontal,
      physics: const ClampingScrollPhysics(),
      children: makeRestaurantList(),
    );
  }

  Widget makeRestaurantsInfo(var url, var name, var description) {
    return Container(
      color: Colors.grey.shade200,
      width: 150,
      margin: EdgeInsets.all(3),
      child: Column(
        children: [
          Expanded(
            child: Image.asset(
              url,
            ),
          ),
          Text(
            name,
            style: const TextStyle(fontSize: 13, fontWeight: FontWeight.w900),
          ),
          Expanded(
            child: Text(
              description,
            ),
          ),
          generatingStar()
        ],
      ),
    );
  }

  Widget makeGridRestaurant(){
    return Container(
      margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
      child: GridView(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 0.5,
          crossAxisSpacing: 1,
          mainAxisSpacing: 1,
        ),
        children: restaurant,
      ),
    );
  }
}
